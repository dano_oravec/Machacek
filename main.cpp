#include <bits/stdc++.h>
#include "Game.h"
#include "Client.h"

using namespace std;

//#define TEST
#define HUMAN
#define PRINT true

#ifndef TEST
bool move_valid(pair<int, int> move, Game &G) {
    return G.acceptable_values_set.find(move) != G.acceptable_values_set.end()
           && (G.current_bid == make_pair(-1, -1) ? true : G.value_priority[move] >= G.value_priority[G.current_bid]);
}

int main() {
    srand((unsigned) time(NULL));
#ifdef HUMAN
    int best_defense_risk = 57, best_attack_risk = 27;
#else
    int best_defense_risk = 50, best_attack_risk = 50;
    int iterations = 10;
    for(int sure = 0; sure < iterations; sure++) {
        cout << sure + 1 << " / " << iterations
             << " | attack_risk: " << best_attack_risk << " | defense_risk: " << best_defense_risk << "\n";
        for (int x = 0; x < 100; x++) {
            for(int y = 0; y < 100; y++) {
                int defrisk = rand() % 91 + 10;
                int attrisk = rand() % 91 + 10;
#endif
                Game G;
                bool first = (bool) (rand() % 2);
                Client first_player(best_defense_risk, best_attack_risk, 14);
#ifndef HUMAN
                Client second_player(defrisk, attrisk, 14);
                int score_of_first = 0;
#endif
                bool finished, last_starter;
                last_starter = first;
                int rounds = 1000;
                for (int r = 0; r < rounds; r++) {
                    finished = false;
                    G = Game();
                    first = !last_starter;
                    last_starter = first;
                    bool first_round = true;
                    while (!finished) {
                        if (first) {
                            pair<pair<int, int>, pair<int, int>> bid = first_player.make_move(G);
                            if (PRINT) cout << "Computer bids " << bid.first.first << ' ' << bid.first.second << '\n';
                            if (bid.first == make_pair(-1, -1)) {
                                if (G.value_priority[G.true_bid] != G.value_priority[G.current_bid]) {
                                    if (PRINT)
                                        cout << "Actual value was " << G.true_bid.first << ' ' << G.true_bid.second
                                             << '\n';
                                    if (PRINT) cout << "Computer won!\n";
                                    finished = true;
#ifndef HUMAN
                                    score_of_first++;
#endif
                                } else {
                                    if (PRINT)
                                        cout << "Bid was " << G.current_bid.first << ' ' << G.current_bid.second
                                             << ", actual value was " << G.true_bid.first << ' ' << G.true_bid.second
                                             << '\n';
#ifdef HUMAN
                                    cout << "You won!\n";
#else
                                    if (PRINT) cout << "COMPUTER2 won!\n";
                                    score_of_first--;
#endif
                                    finished = true;
                                }
                            }
                            first = !first;
                            G.current_bid = bid.first;
                            G.true_bid = bid.second;
                            G.bid_history.push_back(bid.first);
                        } else {
#ifdef HUMAN
                            bool trust;
                            if (!first_round) {
                                cout << "Do you trust him?\n";
                                cin >> trust;
                            }
                            else trust = true;
                            pair<pair<int, int>, pair<int, int>> bid;
                            pair<int, int> now;
                            if (trust) {
                                now = G.throw_dice();
                                cout << "You have " << now.first << ' ' << now.second << "\n";
                                bid.second = now;
                                bid.first = make_pair(1, 2); // Just something invalid
                                while (!move_valid(bid.first, G)) {
                                    cout << "Your bid: " << '\n';
                                    cin >> bid.first.first >> bid.first.second;
                                }
                            } else {
                                bid.first = {-1, -1};
                            }
                            cout << "You bid " << bid.first.first << ' ' << bid.first.second << '\n';
                            if (bid.first == make_pair(-1, -1)) {
                                if (G.value_priority[G.true_bid] != G.value_priority[G.current_bid]) {
                                    cout << "Actual value was " << G.true_bid.first << ' ' << G.true_bid.second << '\n';
                                    cout << "You won!\n";
                                    finished = true;
                                } else {
                                    cout << "Bid was " << G.current_bid.first << ' ' << G.current_bid.second
                                         << ", actual value was " << G.true_bid.first << ' ' << G.true_bid.second << '\n';
                                    cout << "Computer won!\n";
                                    finished = true;
                                }
                            }
                            first = !first;
                            G.current_bid = bid.first;
                            G.true_bid = bid.second;
                            G.bid_history.push_back(bid.first);
#else
                            pair<pair<int, int>, pair<int, int>> bid = second_player.make_move(G);
                            if (PRINT) cout << "COMPUTER2 bids " << bid.first.first << ' ' << bid.first.second << '\n';
                            if (bid.first == make_pair(-1, -1)) {
                                if (G.value_priority[G.true_bid] != G.value_priority[G.current_bid]) {
                                    if (PRINT)
                                        cout << "Actual value was " << G.true_bid.first << ' ' << G.true_bid.second
                                             << '\n';
                                    if (PRINT) cout << "COMPUTER2 won!\n";
                                    finished = true;
                                    score_of_first--;
                                } else {
                                    if (PRINT)
                                        cout << "Bid was " << G.current_bid.first << ' ' << G.current_bid.second
                                             << ", actual value was " << G.true_bid.first << ' ' << G.true_bid.second
                                             << '\n';
                                    if (PRINT) cout << "Computer won!\n";
                                    finished = true;
                                    score_of_first++;
                                }
                            }
                            first = !first;
                            G.current_bid = bid.first;
                            G.true_bid = bid.second;
                            G.bid_history.push_back(bid.first);
#endif

                        }
                        first_round = false;
                        G.round_number++;
                    }
                    if (PRINT) cout << "\n----------------- NEXT ROUND -------------------\n";
                    if (G.round_number > 10000)
                        break;
                }
#ifndef HUMAN
                if(PRINT) cout << "SCORE OF FIRST: " << score_of_first << "\n";
                if (score_of_first < -rounds / 10) {
                    best_defense_risk = defrisk;
                    best_attack_risk = attrisk;
                }
            }
        }
    }
    cout << "BEST_ATTACK_RISK: " << best_attack_risk << " BEST_DEFENSE_RISK: " << best_defense_risk << "\n";
#endif

    return 0;
}
#endif

#ifdef TEST
int main() {
    Game g;
    for(int i = 0; i < 100; i++) {
        auto t = g.throw_dice();
        cout << t.first << ' ' << t.second << '\n';
    }

    return 0;
}
#endif