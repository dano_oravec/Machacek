//
// Created by danza on 3/2/18.
//

#ifndef MACHACEK_GAME_H
#define MACHACEK_GAME_H

#include <bits/stdc++.h>

using namespace std;

class Game {
public:
    int round_number;
    pair<int, int> current_bid;
    pair<int, int> true_bid;
    vector<pair<int, int>> sorted_acceptable_values;
    map<pair<int, int>, int> value_priority;
    map<int, pair<int, int>> combination_from_priority;
    set<pair<int, int>> acceptable_values_set;
    vector<pair<int, int>> bid_history;
    map<pair<int, int>, int> success_probability;
    Game();
    pair<int, int> throw_dice();
};


#endif //MACHACEK_GAME_H
