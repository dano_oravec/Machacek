//
// Created by danza on 3/2/18.
//

#ifndef MACHACEK_CLIENT_H
#define MACHACEK_CLIENT_H

#include <bits/stdc++.h>
#include "Game.h"

using namespace std;

class Client {
public:
    int risk_defense, risk_attack, epsilon;
    Client(int _risk_defense, int _risk_attack, int _epsilon);
    pair<pair<int, int>, pair<int, int>> make_move(Game &game);
    pair<int, int> lie(Game &game);
    bool trust(Game &game);
};


#endif //MACHACEK_CLIENT_H
