//
// Created by danza on 3/2/18.
//

#include "Game.h"
#include <bits/stdc++.h>

using namespace std;

Game::Game() {
    sorted_acceptable_values = {
            {3, 1}, {3, 2},
            {4, 1}, {4, 2}, {4, 3},
            {5, 1}, {5, 2}, {5, 3}, {5, 4},
            {6, 1}, {6, 2}, {6, 3}, {6, 4}, {6, 5},
            {1, 1}, {2, 2}, {3, 3}, {4, 4}, {5, 5}, {6, 6},
            {2, 1}
    };
    for(int i = 0; i < sorted_acceptable_values.size(); i++) {
        value_priority[sorted_acceptable_values[i]] = i;
        combination_from_priority[i] = sorted_acceptable_values[i];
        acceptable_values_set.insert(sorted_acceptable_values[i]);
    }
    current_bid = {-1, -1};
    true_bid = {-1, -1};
    round_number = 1;

    vector<double> p;
    for(int i = 3; i <= 6; i++)
        for (int j = 1; j < i; j++)
            p.push_back(1.0 / 18.0);
    for(int i = 1; i <= 6; i++)
        p.push_back(1.0 / 36.0);
    p.push_back(1.0 / 18.0);
    for(int i = p.size() - 2; i >= 0; i--)
        p[i] += p[i + 1];
    int where = 0;
    for(int i = 3; i <= 6; i++)
        for(int j = 1; j < i; j++)
            success_probability[make_pair(i, j)] = (int)(round(p[where++] * 100.0));
    for(int i = 1; i <= 6; i++)
        success_probability[make_pair(i, i)] = (int)(round(p[where++] * 100.0));
    success_probability[make_pair(2, 1)] = (int)(round(p[where] * 100.0));
}

pair<int, int> Game::throw_dice() {
    int first = rand() % 6 + 1, second = rand() % 6 + 1;
    if(first < second) swap(first, second);
    return {first, second};
}