//
// Created by danza on 3/2/18.
//

#include "Client.h"

using namespace std;

Client::Client(int _risk_defense, int _risk_attack, int _epsilon)
        : risk_defense(_risk_defense), risk_attack(_risk_attack), epsilon(_epsilon) {}

pair<int, int> say_random_higher(Game &game) {
    pair<int, int> lowest = game.current_bid;
    int lowest_value = game.value_priority[lowest];
    int highest_value = game.acceptable_values_set.size() - 1;
    int diff = highest_value - lowest_value;
    return game.combination_from_priority[lowest_value + (diff ? rand() % diff : 0)];
};

pair<int, int> say_one_higher(Game &game) {
    if(game.value_priority[game.current_bid] == game.acceptable_values_set.size())
        return {2, 1};
    return game.combination_from_priority[game.value_priority[game.current_bid] + 1];
};

// TODO program a bot here
// Return pair(choice, actual), where choice is your bid and actual is true value on die
pair<pair<int, int>, pair<int, int>> Client::make_move(Game &game) {
    // -------------- DO NOT CHANGE ------------- //
    if(!trust(game))
        return {{-1, -1}, {-1, -1}};
    pair<int, int> actual = game.throw_dice();
    pair<int, int> choice;
    // -------------- UNTIL HERE ------------- //

    // What if we have to lie
    if (game.value_priority[actual] < game.value_priority[game.current_bid])
        choice = lie(game);
    else
        choice = actual; // Wa can also lie, but why? Maybe it's goot at the beginning...

    // -------- DO NOT CHANGE THIS -------- //
    return make_pair(choice, actual);
}

// TODO program a bot here
pair<int, int> Client::lie(Game &game) {
    return say_random_higher(game);
}

// TODO program a bot here
bool Client::trust(Game &game) {
    // If we are starting a new round
    if(game.current_bid == make_pair(-1, -1))
        return true;
    // Just check probability of him throwing something higher than my last bid
    bool opponent_success = true, my_success;
    if(game.bid_history.size() > 2)
        opponent_success = game.success_probability[game.bid_history[game.bid_history.size() - 2]]
                           >= (risk_defense + (rand() % epsilon) * (rand() % 2 ? 1 : -1));
    my_success = game.success_probability[game.current_bid]
                 >= (risk_attack + (rand() % epsilon) * (rand() % 2 ? 1 : -1));
    return opponent_success && my_success;
}